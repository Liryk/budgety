var budgetController = (function budgetController() {
    var id = 0;

    var Expense = function(id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    }

    var Income = function(id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    }

    var data = {
        allItems: {
            exp: [],
            inc: [],
        },
        totals: {
            exp: 0,
            inc: 0,
        }
    }

    return {
        addItem(type, des, val) {
            let newItem;
            if (type === "inc") {
                newItem = new Income(++id, des, val);
            } else if (type === "exp") {
                newItem = new Expense(++id, des, val);
            } else {
                throw `Unknow operation: ${type}`;
            }
            data.allItems[type].push(newItem);
            return newItem;
        },
        printData() {
            console.log(data);
        }
    }

})();

var uiController = (function uiController(){
    const DOMStrings = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn',
        incomeList: '.income__list',
        expensesList: '.expenses__list',
    }

    return {
        clearFields() {
            //Rewrited as in lesson to use different methods described in lesson.
            const fieldsList = document.querySelectorAll(`${DOMStrings.inputDescription},${DOMStrings.inputValue}`);
            //querySelectorAll returns the list object not an array.
            //So to transform list to an array call the slice method on it trought the 'call' function to trick slice method and pass list object to it.
            const fieldsArray = Array.prototype.slice.call(fieldsList);
            fieldsArray.forEach(
                (current, index, array) => {
                    current.value = "";
                }
            );
            fieldsArray[0].focus();
        },
        getInput: function() {
            return { 
                type: document.querySelector(DOMStrings.inputType).value,
                description: document.querySelector(DOMStrings.inputDescription).value,
                value: document.querySelector(DOMStrings.inputValue).value,
            };
        },
        addListItem(obj, type) {
            let html, container;
            if (type === 'inc') {
                container = document.querySelector(DOMStrings.incomeList);
                html = `<div class="item clearfix" id="income-${obj.id}">
                            <div class="item__description">${obj.description}</div>
                            <div class="right clearfix">
                                <div class="item__value">+ ${obj.value}</div>
                                <div class="item__delete">
                                    <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>
                                </div>
                            </div>
                        </div>`;
            } else if (type === 'exp') {
                container = document.querySelector(DOMStrings.expensesList);
                html = `<div class="item clearfix" id="expense-${obj.id}">
                            <div class="item__description">${obj.description}</div>
                            <div class="right clearfix">
                                <div class="item__value">- ${obj.value}</div>
                                <div class="item__percentage">666%</div>
                                <div class="item__delete">
                                    <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>
                                </div>
                            </div>
                        </div>`;
            }
            container.insertAdjacentHTML('beforeend', html);
        },
        DOMStrings: DOMStrings,
    }
})();


var controller = (function controller(budgetCtrl, uiCtrl) {

    function setupEventListeners() {
        const DOM = uiCtrl.DOMStrings;

        document.querySelector(DOM.inputBtn).addEventListener('click', ctrlAddItem);

        document.addEventListener('keydown', function(event) {
            if(event.keyCode === 13 || event.which === 13) {
                ctrlAddItem();
            }
        });
    }
    
    function ctrlAddItem() {
        const input = uiCtrl.getInput();
        if(input.description && input.value) {
            const newItem = budgetCtrl.addItem(input.type, input.description, input.value);
            uiCtrl.addListItem(newItem, input.type);
            uiCtrl.clearFields();
            budgetCtrl.printData();
        }
    }
    
    return {
        init: function() {
            console.log('Application has started.');
            setupEventListeners();
        }
    };

})(budgetController, uiController);

controller.init();